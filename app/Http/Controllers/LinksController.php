<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\User;
use App\Link;
use App\School;

class LinksController extends Controller
{
    public function create()
    {
        $categories = Category::active()->get();
        return view('links.create', compact(['categories']));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'category' => 'required',
            'link' => 'required',
        ],[
            'category.required' => 'Выберите предмет',
            'link.required' => 'Укажите ссылку',
        ]);

        $link = Link::create([
            'category_id' => $request->input('category'),
            'link' => $request->input('link'),
            'school_id' => auth()->user()->school->id,
        ]);

        $category = Category::find($request->input('category'));

        return redirect(route('links.result', $category));
    }

    public function result(Category $category)
    {
        $links = Link::where('category_id', $category->id)->where('school_id', auth()->user()->school_id)->get();

        return view('links.result', compact(['links', 'category']));
    }

    public function list()
    {
        $schools = School::all();
        $categories = Category::all();
        $links = Link::all();

        return view('links.list', compact(['schools', 'categories', 'links']));
    }
}
