<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\School;
use \App\Category;
use Illuminate\Support\Facades\Storage;

class SchoolsController extends Controller
{
    public function index() 
    {
        $schools = \App\School::all();
        return view('schools.index', compact(['schools']));
    }

    public function create() 
    {
        return view('schools.create');
    }

    public function store(Request $request) 
    {
        $school = School::create([
            'name' => $request->input('school'),
            'folder' => $request->input('folder'),
        ]);

        $categories = Category::all();
        foreach ($categories as $category) {
            Storage::disk('local')->makeDirectory('pdfs/' . $category->folder . '/' . $school->folder);
        }

        return redirect(route('schools.index'));
    }

    public function edit(School $school) 
    {
        return view('schools.edit', compact(['school']));
    }

    public function update(Request $request, School $school) 
    {
        $school->name = $request->input('school');
        $school->update();

        return redirect(route('schools.index'));
    }

    public function destroy(School $school)
    {
        $school->delete();
        return redirect(route('schools.index'));
    }
}
