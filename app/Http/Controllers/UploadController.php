<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\School;
use App\User;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function upload()
    {
        $categories = Category::active()->get();
        return view('upload.form', compact(['categories']));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'files.*' => 'file|mimes:pdf|max:3072',
            'category' => 'required',
        ],[
            'category.required' => "Необходимо выбрать предмет",
            'files.*.mimes' => "Неверный формат файлов, убедитесь что все передаваемые файл имеют форма pdf",
            'files.*.max' => "Размер файла не должен превышать 3Мб",
        ]);

        $files = $request->file('files');
        $category = Category::find($request->input('category'));

        if ($category && $files) {
            foreach ($files as $file) {
                $file->storeAs('pdfs/' . $category->folder . '/' . auth()->user()->school->folder . '/', $file->getClientOriginalName());
            }
        }

        return redirect(route('scans.result', $category));
    }

    public function uploadResult(Category $category)
    {
        $files = $category->getFileListForSchool(auth()->user()->school);
        
        $names = array_map(function($item) {
            return $item->getFilename();
        }, $files);
        return view('upload.result', compact(['category', 'names']));
    }

    public function status()
    {
        $data = [];
        $categories = Category::all();

        foreach ($categories as $category) {
            $data[$category->name] = $category->getCountFilesForSchool(auth()->user()->school);
        }
        
        return view('status', compact('data'));
    }

    public function dashboard()
    {
        $data = [];
        $categories = Category::all();
        $schools = School::all();

        foreach ($categories as $category) {
            foreach ($schools as $school) {
                $data[$category->name][$school->name] = $category->getCountFilesForSchool($school);
                $data[$category->name]['folder'] = $category->folder;
            }
        }
        return view('dashboard', compact('data'));
    }
}
