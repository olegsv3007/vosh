<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\School;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    public function index() 
    {
        $categories = \App\Category::all();
        return view('categories.index', compact(['categories']));
    }

    public function create() 
    {
        return view('categories.create');
    }

    public function store(Request $request) 
    {
        $category = Category::create([
            'name' => $request->input('category'),
            'folder' => $request->input('folder'),
        ]);

        Storage::disk('local')->makeDirectory('pdfs/' . $category->folder);

        $schools = School::all();
        foreach ($schools as $school) {
            Storage::disk('local')->makeDirectory('pdfs/' . $category->folder . '/' . $school->folder);
        }

        return redirect(route('categories.index'));
    }

    public function edit(Category $category) 
    {
        return view('categories.edit', compact(['category']));
    }

    public function update(Request $request, Category $category) 
    {
        $category->name = $request->input('category');
        $category->active = $request->input('active') ?? 0;
        $category->update();

        return redirect(route('categories.index'));
    }

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect(route('categories.index'));
    }
}
