<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use \App\School;
use \App\User;
use \App\Role;

class UsersController extends Controller
{
    public function index() 
    {
        $users = User::all();
        return view('users.index', compact(['users']));
    }

    public function create() 
    {
        $schools = School::all();
        $roles = Role::all();
        return view('users.create', compact(['schools', 'roles']));
    }

    public function store(Request $request) 
    {
        $user = User::create([
            'name' => $request->input('login'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'school_id' => $request->input('school'),
        ]);

        $user->roles()->attach($request->input('roles'));

        return redirect(route('users.index'));
    }

    public function edit(User $user) 
    {
        $schools = School::all();
        $roles = Role::all();
        return view('users.edit', compact(['user', 'schools', 'roles']));
    }

    public function update(Request $request, User $user) 
    {
        $user->name = $request->input('login');
        $user->email = $request->input('email');
        $user->school_id = $request->input('school');

        if ($request->has('password') && $request->input('password') != '') {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();
        $user->roles()->sync($request->input('roles'));

        return redirect(route('users.index'));
    }
}
