<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\School;
use App\Report;
use App\User;

class ReportsController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $schools = School::with('reports')->get();
        return view('reports.index', compact(['categories', 'schools']));
    }

    public function create()
    {
        $categories = Category::active()->get();
        return view('reports.create', compact(['categories']));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'category_id' => 'required',
            'seven' => 'numeric|min:0',
            'eight' => 'numeric|min:0',
            'nine' => 'numeric|min:0',
            'ten' => 'numeric|min:0',
            'eleven' => 'numeric|min:0',
            'ovz' => 'numeric|min:0',
        ], [
            'category_id.required' => 'Укажите предмет',
            'seven.min' => 'Значение не может быть меньше 0',
            'eight.min' => 'Значение не может быть меньше 0',
            'nine.min' => 'Значение не может быть меньше 0',
            'ten.min' => 'Значение не может быть меньше 0',
            'eleven.min' => 'Значение не может быть меньше 0',
            'ovz.min' => 'Значение не может быть меньше 0',
            'seven.numeric' => 'Укажите только число',
            'eight.numeric' => 'Укажите только число',
            'nine.numeric' => 'Укажите только число',
            'ten.numeric' => 'Укажите только число',
            'eleven.numeric' => 'Укажите только число',
            'ovz.numeric' => 'Укажите только число',
        ]);

        $validatedData['school_id'] = auth()->user()->school->id;


        $category = Category::find($request->input('category_id'));
        $school = auth()->user()->school;
        \DB::table('reports')->updateOrInsert([
            'category_id' => $category->id,
            'school_id' => $school->id,
        ], [
            'seven' => $validatedData['seven'],
            'eight' => $validatedData['eight'],
            'nine' => $validatedData['nine'],
            'ten' => $validatedData['ten'],
            'eleven' => $validatedData['eleven'],
            'ovz' =>  $validatedData['ovz'],
        ]);

        $report = Report::where('category_id', $category->id)->where('school_id', $school->id)->first();

        return view('reports.result', compact(['report', 'school', 'category']));
    }
}
