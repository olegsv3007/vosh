<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = ['name', 'folder'];
    public $timestamps = false;

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
