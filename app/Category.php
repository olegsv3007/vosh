<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{
    protected $fillable = ['name', 'folder'];
    public $timestamps = false;

    public function getFileListForSchool($school)
    {
        $files = \File::files(Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . 'pdfs/' . $this->folder . '/' . $school->folder . '/');
        return $files;
    }

    public function getCountFilesForSchool($school)
    {
        $countFiles =  count($this->getFileListForSchool($school));
        return $countFiles;
    }

    public function getSummaryCountFiles()
    {
        $schools = School::all();
        $countFiles = $schools->sum(function($school) {
            return $this->getCountFilesForSchool($school);
        });
        return $countFiles;
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
