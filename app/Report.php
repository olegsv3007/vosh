<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['school_id', 'category_id', 'seven', 'eight', 'nine', 'ten', 'eleven', 'ovz'];

    public function getSummaryMembers()
    {
        return $this->seven + $this->eight + $this->nine + $this->ten + $this->eleven;
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
 
    public function school()
    {
        return $this->belongsTo('App\School');
    }
}
