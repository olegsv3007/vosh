<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->unsignedBigInteger('school_id');
            $table->unsignedBigInteger('category_id');
            $table->unsignedSmallInteger('seven');
            $table->unsignedSmallInteger('eight');
            $table->unsignedSmallInteger('nine');
            $table->unsignedSmallInteger('ten');
            $table->unsignedSmallInteger('eleven');
            $table->unsignedSmallInteger('ovz');
            $table->timestamps();

            $table->index(['school_id', 'category_id']);
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
