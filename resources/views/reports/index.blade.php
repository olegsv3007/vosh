@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h2>Отчет по количеству участников</h2>
            @foreach($categories as $category)
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#{{ $category->folder }}" aria-expanded="true" aria-controls="collapseOne">
                        {{ $category->name }}
                        </button>
                    </h2>
                    </div>

                    <div id="{{ $category->folder }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Школа</th>
                                    <th scope="col">7 класс</th>
                                    <th scope="col">8 класс</th>
                                    <th scope="col">9 класс</th>
                                    <th scope="col">10 класс</th>
                                    <th scope="col">11 класс</th>
                                    <th scope="col">Детей-инвалидов и детей с ОВЗ</th>
                                    <th scope="col">Итого</th>
                                    <th scope="col">Загруженных файлов</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($schools as $school) 
                                <tr @if(!$report = $school->reports->where('category_id', $category->id)->first()) class="bg-warning" @endif>
                                    @if (!$report)
                                        <th scope="col">{{ $school->name }}</th>
                                        <td scope="col" class="text-center" colspan=7>Отчет не отправлялся</td>
                                        <th scope="col" class="text-center">{{ $category->getCountFilesForSchool($school) }}</th>
                                    @else
                                    <th scope="col">{{ $school->name }}</th>
                                    <td scope="col" class="text-center">{{ $report->seven }}</td>
                                    <td scope="col" class="text-center">{{ $report->eight }}</td>
                                    <td scope="col" class="text-center">{{ $report->nine }}</td>
                                    <td scope="col" class="text-center">{{ $report->ten }}</td>
                                    <td scope="col" class="text-center">{{ $report->eleven }}</td>
                                    <td scope="col" class="text-center">{{ $report->ovz }}</td>
                                    <td scope="col" class="{{ $report->getSummaryMembers() == $category->getCountFilesForSchool($school) ? '' : 'bg-danger text-light'}} text-center">{{ $report->getSummaryMembers() }}</td>
                                    <th scope="col" class="text-center">{{ $category->getCountFilesForSchool($school) }}</th>
                                    @endif
                                </tr>
                            @endforeach
                                <tr class="bg-success">
                                    <th scope="col">Итого</th>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('seven') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('eight') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('nine') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('ten') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('eleven') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum('ovz') }}</td>
                                    <td scope="col" class="text-center">{{ $category->reports->sum(function($report) {
                                        return $report->seven + $report->eight + $report->nine + $report->ten + $report->eleven;
                                    }) }}</td>
                                    <th scope="col" class="text-center">{{ $category->getSummaryCountFiles() }}</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            @endforeach

            
        </div>
    </div>
</div>
@endsection
