@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Отчет по предмету "{{ $category->name }}" от "{{ $school->name }}"  отправлен</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Количество участников</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">7 Класс:</th>
                        <td>{{ $report->seven }}</td>
                    </tr>
                    <tr>
                        <th scope="row">8 Класс:</th>
                        <td>{{ $report->eight }}</td>
                    </tr>
                    <tr>
                        <th scope="row">9 Класс:</th>
                        <td>{{ $report->nine }}</td>
                    </tr>
                    <tr>
                        <th scope="row">10 Класс:</th>
                        <td>{{ $report->ten }}</td>
                    </tr>
                    <tr>
                        <th scope="row">11 Класс:</th>
                        <td>{{ $report->eleven }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Всего:</th>
                        <td>{{ $report->getSummaryMembers() }}</td>
                    </tr>
                    <tr>
                        <th scope="row">Из них, детей-инвалидов и детей с ОВЗ:</th>
                        <td>{{ $report->ovz }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection