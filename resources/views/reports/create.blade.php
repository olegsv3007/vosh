@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Отправка отчета {{ auth()->user()->school->name }}</h2>
        <div class="alert alert-warning" role="alert">
            Если вы нечаянно отправили неверные данные то просто отправте отчет еще раз. Новые данные заменят те, которые были отправлены ранее.
        </div>
            <form action="{{ route('reports.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="category">Выберите предмет</label>
                    <select class="form-control" id="category" name="category_id" require>
                        @forelse($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @empty
                        @endforelse
                    </select>
                    @error('category_id')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <h3>Количество участников</h3>
                <div class="form-group">
                    <div class="form-group">
                        <label for="seven">7 Класс</label>
                        <input type="number" min="0" value="{{ old('seven') ?? 0 }}" class="form-control members" id="seven" name="seven">
                    </div>
                    @error('seven')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="eight">8 Класс</label>
                        <input type="number" min="0" value="{{ old('eight') ?? 0 }}" class="form-control members" id="eight" name="eight">
                    </div>
                    @error('eight')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="nine">9 Класс</label>
                        <input type="number" min="0" value="{{ old('nine') ?? 0 }}" class="form-control members" id="nine" name="nine">
                    </div>
                    @error('nine')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="ten">10 Класс</label>
                        <input type="number" min="0" value="{{ old('ten') ?? 0 }}" class="form-control members" id="ten" name="ten">
                    </div>
                    @error('ten')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="eleven">11 Класс</label>
                        <input type="number" min="0" value="{{ old('eleven') ?? 0 }}" class="form-control members" id="eleven" name="eleven">
                    </div>
                    @error('eleven')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <h2>Итого: <span id="summary_mebmers">0</span></h2>
                <div class="form-group">
                    <div class="form-group">
                        <label for="ovz">Из них с ОВЗ</label>
                        <input type="number" min="0" value="{{ old('ovz') ?? 0 }}" class="form-control" id="ovz" name="ovz">
                    </div>
                    @error('ovz')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <input type="submit" value="Отправить отчет" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
<script>
    const fields = [...document.querySelectorAll('.members')];
    const summaryField = document.querySelector('#summary_mebmers');
    const reducer = (accumulator, value) => parseInt(accumulator) + parseInt(value);

    function updateField() {
        let values = fields.map(field => field.value);
        summaryField.innerText = values.reduce(reducer);
    }

    fields.map(function(field) {
        field.addEventListener('input', updateField);
    });

    updateField();

</script>
@endsection