@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Статистика</h2>
            @foreach($data as $key=>$value)
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#{{ $value['folder'] }}" aria-expanded="true" aria-controls="collapseOne">
                        {{ $key }}
                        </button>
                    </h2>
                    </div>

                    <div id="{{ $value['folder'] }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Школа</th>
                                    <th scope="col">Количество загруженных файлов</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($value as $category=>$count) 
                            @if($category == 'folder') 
                                @continue
                            @endif
                                <tr @if($count == 0) class="bg-warning" @endif>
                                    <th scope="col">{{ $category }}</th>
                                    <th scope="col">{{ $count }}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            @endforeach

            
        </div>
    </div>
</div>
@endsection
