@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Статистика {{ auth()->user()->school->name }}</h2>
            <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Предмет</th>
                    <th scope="col">Количество загруженных файлов</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $key=>$item)
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $item }}</td>
                </tr>
                @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
