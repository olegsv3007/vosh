@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Загрузка сканов {{ auth()->user()->school->name }}</h2>
            <h3 class="mt-3">В категорию "{{ $category->name }}" всего добавлено файлов: {{ count($names) }}</h3>
            <ul class="list-group list-group-flush">
                @foreach($names as $name)
                <li class="list-group-item">{{ $name }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection