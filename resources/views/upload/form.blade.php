@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Загрузка сканов {{ auth()->user()->school->name }}</h2>
        @if (auth()->user()->school->id == 47)
        <div class="alert alert-danger" role="alert">
            <h2>СОШ №50 Обратите внимание!!!</h2>
            Имя файла должно содержать номер класса(только цифра!), номер ОУ, фамилию и инициалы участника и должно быть записано строго в формате №класса_№школы_Фамилия И.О. (например: 7_22_Иванов И.И.)<br>
        </div>
        @endif
        <div class="alert alert-warning" role="alert">
            Все передаваемые через форму файлы <strong>должны быть в формате PDF</strong>.<br>
            За один раз можно передать <strong>не более 10 файлов</strong>.<br>
            Максимальный размер одного файла <strong>3 МБ</strong>(<a class="alert-link" href="https://tools.pdf24.org/ru/compress-pdf">Онлайн инструмент для сжатия PDF файлов</a>)<br>
            Каждая работа должна быть в <strong>отдельном PDF файле</strong><br>
            На <strong>первой странице</strong> PDF файла должен быть только <strong>лист с регистрационной частью участника</strong><br>
            <strong>Имя файла должно содержать номер класса(только цифра!), номер ОУ, фамилию и инициалы участника и должно быть записано строго в формате №класса_№школы_Фамилия И.О. (например: 7_22_Иванов И.И.)</strong><br>
            При возникновении вопросов пишите на адрес <strong>metod_it@edu.klgd.ru</strong>, в теме письма укажите "ВОШ"<br>
        </div>
            <form action="{{ route('scans.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="category">Выберите предмет</label>
                    <select class="form-control" id="category" name="category" require>
                        @forelse($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @empty
                        @endforelse
                    </select>
                    @error('category')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="scans">Выберите файлы для отправки</label>
                    <input type="file" multiple class="form-control-file" id="scans" name="files[]" accept="application/pdf">
                    @error('files.*')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <input type="submit" value="Загрузить" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection