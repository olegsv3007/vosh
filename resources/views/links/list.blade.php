@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Ссылки</h2>
            @foreach($categories as $category)
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#category-{{ $category->id }}" aria-expanded="true" aria-controls="collapseOne">
                        {{ $category->name }}
                        </button>
                    </h2>
                    </div>

                    <div id="category-{{ $category->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Школа</th>
                                    <th scope="col">Ссылки</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($schools as $school) 
                                <tr>
                                    <th scope="col">{{ $school->name }}</th>
                                    <th scope="col">
                                        @foreach ($links->where('school_id', $school->id)->where('category_id', $category->id) as $link)
                                        <a href="{{ $link->link }}">link</a> <br>
                                        @endforeach
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
            @endforeach

            
        </div>
    </div>
</div>
@endsection
