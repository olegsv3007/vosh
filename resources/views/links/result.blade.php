@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Загрузка ссылок {{ auth()->user()->school->name }}</h2>
            <h3 class="mt-3">В категорию "{{ $category->name }}" всего добавлено ссылок: {{ $links->count() }}</h3>
            <ul class="list-group list-group-flush">
                @foreach($links as $link)
                <li class="list-group-item">{{ $link->link }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection