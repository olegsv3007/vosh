@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Загрузка ссылок на видеозаписи {{ auth()->user()->school->name }}</h2>
        <div class="alert alert-warning" role="alert">
            Видеозаписи загружаются на облачные сервисы, через данную форму отправляются только ссылки на видеозаписи.<br>
            <a class="alert-link" href="{{ Storage::url('upload_video.pdf') }}">Инструкция по загрузке файлов в облако</a>
        </div>
            <form action="{{ route('links.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="category">Выберите предмет</label>
                    <select class="form-control" id="category" name="category" require>
                        @forelse($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @empty
                        @endforelse
                    </select>
                    @error('category')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label for="link">Ссылка</label>
                        <input type="text" class="form-control" id="link" name="link">
                    </div>
                    @error('link')
                    <div class="invalid-feedback d-block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <input type="submit" value="Сохранить ссылку" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection