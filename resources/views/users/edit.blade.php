@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Редактирование Пользователя</h2>
            <form action="{{ route('users.update', $user) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="login">Имя пользователя</label>
                    <input type="text" class="form-control" id="login" name="login" value="{{ $user->name }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group">
                    <label for="school">Выберите учреждение</label>
                    <select class="form-control" id="school" name="school">
                        @forelse($schools as $school)
                        <option value="{{ $school->id }}" {{ $user->school_id == $school->id ? 'selected' : '' }}>{{ $school->name }}</option>
                        @empty
                        @endforelse
                    </select>
                </div>
                <h3>Роли</h3>
                @foreach($roles as $role)
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="role-{{ $role->id }}" name="roles[]" value="{{ $role->id }}" {{ $user->roles->contains($role) ? 'checked' : '' }}>
                    <label class="form-check-label" for="role-{{ $role->id }}">{{ $role->name }}</label>
                </div>   
                @endforeach
                <br>
                <br>
                <input type="submit" value="Сохранить изменения" class="btn btn-primary m-auto mt-5">
            </form>
        </div>
    </div>
</div>
@endsection