@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Пользователи</h2>
        <a href="{{ route('users.create') }}" class="btn btn-primary mt-5 mb-5 float-right">Добавть Пользователя</a>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">Логин</th>
                <th scope="col">Школа</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @forelse($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->school->name ?? ''}}</td>
                    <td><a href="{{ route('users.edit', $user) }}">Редактировать</a></td>
                </tr>
                @empty
                    <td colspan=5>Пусто</td>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection