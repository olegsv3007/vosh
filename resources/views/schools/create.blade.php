@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Создание Школы</h2>
            <form action="{{ route('schools.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="school">Название школы</label>
                    <input type="text" class="form-control" id="school" name="school">
                </div>
                <div class="form-group">
                    <label for="folder">Имя папки</label>
                    <input type="text" class="form-control" id="folder" name="folder">
                </div>
                <input type="submit" value="Создать школу" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection