@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Редактирование Школы</h2>
            <form action="{{ route('schools.update', $school) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="school">Название школы</label>
                    <input type="text" class="form-control" id="school" name="school" value="{{ $school->name }}">
                </div>
                <input type="submit" value="Сохранить изменения" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection