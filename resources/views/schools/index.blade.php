@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Школы</h2>
        <a href="{{ route('schools.create') }}" class="btn btn-primary mt-5 mb-5 float-right">Добавть Школу</a>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Школа</th>
                <th scope="col"></th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @forelse($schools as $school)
                <tr>
                    <th scope="row">{{ $school->id }}</th>
                    <td>{{ $school->name }}</td>
                    <td><a href="{{ route('schools.edit', $school) }}">Редактировать</a></td>
                    <td><a href="" onclick="event.preventDefault();
                                            document.getElementById('remove-school-{{ $school->id }}').submit();">Удалить</a>
                        <form action="{{ route('schools.destroy', $school) }}" method="post" class="d-none" id="remove-school-{{ $school->id }}">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
                @empty
                    <td colspan=4>Пусто</td>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection