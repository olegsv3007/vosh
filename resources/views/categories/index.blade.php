@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Предметы</h2>
        <a href="{{ route('categories.create') }}" class="btn btn-primary mt-5 mb-5 float-right">Добавть предмет</a>
        <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Предмет</th>
                <th scope="col">Активен</th>
                <th scope="col"></th>
                {{--<th scope="col"></th>--}}
                </tr>
            </thead>
            <tbody>
                @forelse($categories as $category)
                <tr>
                    <th scope="row">{{ $category->id }}</th>
                    <td>{{ $category->name }}</td>
                    <td><span class="{{ $category->active ? 'text-success' : 'text-danger' }}">{{ $category->active ? 'Да' : 'Нет' }}</span></td>
                    <td><a href="{{ route('categories.edit', $category) }}">Редактировать</a></td>
                    {{--<td><a href="" onclick="event.preventDefault();
                                            document.getElementById('remove-category-{{ $category->id }}').submit();">Удалить</a>
                        <form action="{{ route('categories.destroy', $category) }}" method="post" class="d-none" id="remove-category-{{ $category->id }}">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>--}}
                </tr>
                @empty
                    <td colspan=4>Пусто</td>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection