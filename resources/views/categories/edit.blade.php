@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Редактирование предмета "{{ $category->name }}"</h2>
            <form action="{{ route('categories.update', $category) }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <label for="category">Название предмета</label>
                    <input type="text" class="form-control" id="category" name="category" value="{{ $category->name }}">
                </div>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="checkbox" value="1" name="active" id="defaultCheck1" {{ $category->active ? 'checked' : '' }}>
                    <label class="form-check-label" for="defaultCheck1">
                        Активность
                    </label>
                </div>
                <input type="submit" value="Сохранить изменения" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection