@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <h2>Создание предмета</h2>
            <form action="{{ route('categories.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="category">Название предмета</label>
                    <input type="text" class="form-control" id="category" name="category">
                </div>
                <div class="form-group">
                    <label for="folder">Имя папки</label>
                    <input type="text" class="form-control" id="folder" name="folder">
                </div>
                <input type="submit" value="Создать предмет" class="btn btn-primary m-auto">
            </form>
        </div>
    </div>
</div>
@endsection