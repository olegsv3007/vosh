<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::middleware(['auth', 'auth.admin'])->group(function() {
    Route::get('/', 'UploadController@dashboard')->name('main');
    
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/schools', 'SchoolsController');
    Route::resource('/users', 'UsersController');

    Route::get('/dashboard', 'UploadController@dashboard')->name('dashboard');

    Route::get('/links/list', 'LinksController@list')->name('links.list');

    Route::get('/report', 'ReportsController@index')->name('report');
});

Route::middleware(['auth', 'auth.school'])->group(function() {
    Route::get('/', 'UploadController@upload')->name('scans.upload');

    Route::get('/upload', 'UploadController@upload')->name('scans.upload');
    Route::post('/upload', 'UploadController@store')->name('scans.store');
    Route::get('/uploadResult/{category}', 'UploadController@uploadResult')->name('scans.result');

    Route::get('/links/create', 'LinksController@create')->name('links.create');
    Route::post('/links/store', 'LinksController@store')->name('links.store');
    Route::get('/links/result/{category}', 'LinksController@result')->name('links.result');

    Route::get('/status', 'UploadController@status')->name('status');

    Route::get('/report/create', 'ReportsController@create')->name('repotrs.create');
    Route::post('/report/store', 'ReportsController@store')->name('reports.store');
});

